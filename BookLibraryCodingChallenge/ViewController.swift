//
//  ViewController.swift
//  BookLibraryCodingChallenge
//
//  Created by Mohit Gupta on 20/06/1942 Saka.
//  Copyright © 1942 Mohit Gupta. All rights reserved.
//
 

import UIKit

class ViewController: UIViewController {
    
    let rest = RestManager()
    private var itemRecords = [result]()
    @IBOutlet weak var TableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    func configure(){
        TableView.estimatedRowHeight = 70
        TableView.rowHeight = UITableView.automaticDimension
        self.navigationController?.navigationBar.tintColor = .white
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Payment"
        self.navigationController?.navigationBar.titleTextAttributes =
        [NSAttributedString.Key.foregroundColor: UIColor.white,
         NSAttributedString.Key.font: UIFont(name: appFont, size: appHeadingFontSize)!]
        
        if Utility.isNetworkAvailable(controller: self){
            getUsersList()
        }
        
        
    }
    
    
    func getUsersList() {
        self.showLoader()
        guard let url = URL(string: "http://skunkworks.ignitesol.com:8000/books/") else { return }
        
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            
            do{
                if let data = results.data {
                    
                    
                    DispatchQueue.main.async {
                        self.hideLoader()
                        
                        self.itemRecords = self.parseJsonData(data: data)
                        
                        print(self.itemRecords)
                        self.TableView.reloadData()
                        
                    }
                }
            }
            
        }
    }
    
    func parseJsonData(data: Data) -> [result] {
        
        var loans = [result]()
        
        let decoder = JSONDecoder()
        
        do {
            let loanDataStore = try decoder.decode(ItemObject.self, from: data)
            print(loanDataStore)
            loans = loanDataStore.results
            
        } catch {
            print(error)
        }
        
        return loans
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return itemRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomTableViewCell
        cell.wrapData(selectedObject: self.itemRecords[indexPath.row] )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let editAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "bookViewController") as? bookViewController {
            print(self.itemRecords[indexPath.row])
            editAddressVC.bannerData = self.itemRecords[indexPath.row].subjects
            editAddressVC.currentObject = self.itemRecords[indexPath.row]
            self.navigationController?.pushViewController(editAddressVC, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}





class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftIconImageView: UIImageView!
    @IBOutlet weak var rightIconImageView: UIImageView!
    
    @IBOutlet var titleLbl: UILabel!{
        didSet {
            self.titleLbl.font = UIFont(name: appFont, size: appHeadingFontSize)
            self.titleLbl.text = self.titleLbl.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
        let margins = UIEdgeInsets(top: 15, left: 5, bottom: 5, right: 5)
        contentView.frame = contentView.frame.inset(by: margins)
    }
    
    func wrapData(selectedObject: result){
        self.titleLbl.text = selectedObject.title
    }
}
