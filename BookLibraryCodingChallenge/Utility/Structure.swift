 //
 //  AppUtility.swift
 //  BookLibraryCodingChallenge
 //
 //  Created by Mohit Gupta on 20/06/1942 Saka.
 //  Copyright © 1942 Mohit Gupta. All rights reserved.
 //
 
 import Foundation
 import UIKit
 
 
 struct DataStore: Codable {
    var loans: [ItemObject]
 }
 
 
 struct ItemObject:Codable {
    
    var count:Int = 0
    var next: String = ""
    var previous: String = ""
    var results: [result]
    
    
    enum CodingKeys: String, CodingKey {
        case count = "count"
        case next = "next"
        case previous = "previous"
        case results = "results"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decode(Int.self, forKey: .count)
        next = try values.decode(String.self, forKey: .next)
        previous = try values.decodeIfPresent(String.self, forKey: .previous) ?? ""
        results = try values.decode([result].self, forKey: .results)
        
    }
 }
 
 
 
 struct result:Codable {
    
    var id:Int = 0
    //    var authors: [[Dictionary<String, String>]]
    var bookshelves: [String]
    var download_count: Int = 0
    var formats: [String:String]
    var languages: [String]
    var media_type: String = ""
    var subjects: [String]
    var title: String = ""
    
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        //        case authors = "authors"
        case bookshelves = "bookshelves"
        
        case download_count = "download_count"
        case formats = "formats"
        case languages = "languages"
        
        case media_type = "media_type"
        case subjects = "subjects"
        case title = "title"
        
    }
    
    
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        //        authors = try values.decode([[Dictionary<String, String>]].self, forKey: .authors)
        bookshelves = try values.decode([String].self, forKey: .bookshelves)
        
        
        download_count = try values.decode(Int.self, forKey: .download_count)
        //         formats = try values.decode(format.self, forKey: .formats)
        
        formats = try values.decode([String:String].self, forKey: .formats)
        
        languages = try values.decode([String].self, forKey: .languages)
        
        
        media_type = try values.decode(String.self, forKey: .media_type)
        subjects = try values.decode([String].self, forKey: .subjects)
        title = try values.decode(String.self, forKey: .title)
    }
    
    
    
 }
 
 
 //  struct author:Codable {
 //
 //     var birth_year:Int = 0
 //     var death_year:Int = 0
 //     var name: String = ""
 //
 //
 //     enum CodingKeys: String, CodingKey {
 //         case birth_year = "birth_year"
 //         case death_year = "death_year"
 //         case name = "name"
 //
 //     }
 //
 //
 //     init(from decoder: Decoder) throws {
 //         let values = try decoder.container(keyedBy: CodingKeys.self)
 //         birth_year = try values.decode(Int.self, forKey: .birth_year)
 //         death_year = try values.decode(Int.self, forKey: .death_year)
 //         name = try values.decode(String.self, forKey: .name)
 //     }
 //
 //
 //  }
 
 
 
 
 
 struct format: Codable {
    
    var applicationxmobipocketebook:  URL
    var applicationpdf:  URL
    var textplaincharsetusascii:  URL
    var textplaincharsetutf8:  URL
    var applicationrdfxml:  URL
    var applicationzip:  URL
    var applicationepubzip:  URL
    var texthtmlcharsetutf8:  URL
    var imagejpeg: URL
    
    
    enum CodingKeys: String, CodingKey {
        case applicationxmobipocketebook = "application/x-mobipocket-ebook"
        case applicationpdf = "application/pdf"
        case textplaincharsetusascii = "text/plain; charset=us-ascii"
        case textplaincharsetutf8 = "text/plain; charset=utf-8"
        case imagejpeg = "image/jpeg"
        case applicationrdfxml = "application/rdf+xml"
        case applicationzip = "application/zip"
        case applicationepubzip = "application/epub+zip"
        case texthtmlcharsetutf8 = "text/html; charset=utf-8"
    }
    
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        applicationxmobipocketebook = try values.decode(URL.self, forKey: .applicationxmobipocketebook)
        
        applicationpdf = try values.decode(URL.self, forKey: .applicationpdf)
        
        textplaincharsetusascii = try values.decode(URL.self, forKey: .textplaincharsetusascii)
        
        textplaincharsetutf8 = try values.decode(URL.self, forKey: .textplaincharsetutf8)
        
        imagejpeg = try values.decode(URL.self, forKey: .imagejpeg)
        
        applicationrdfxml = try values.decode(URL.self, forKey: .applicationrdfxml)
        
        applicationzip = try values.decode(URL.self, forKey: .applicationzip)
        
        applicationepubzip = try values.decode(URL.self, forKey: .applicationepubzip)
        
        texthtmlcharsetutf8 = try values.decode(URL.self, forKey: .texthtmlcharsetutf8)
    }
    
    
 }
 
 
 
 
