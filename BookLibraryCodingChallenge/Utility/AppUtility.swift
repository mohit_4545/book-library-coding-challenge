//
//  AppUtility.swift
//  BookLibraryCodingChallenge
//
//  Created by Mohit Gupta on 20/06/1942 Saka.
//  Copyright © 1942 Mohit Gupta. All rights reserved.
//

import UIKit
var appFont: String = "Montserrat-Regular"
var appFontSize: CGFloat = 12.0
var appHeadingFontSize: CGFloat = 16.0

let SCREEN_WIDTH: CGFloat = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT: CGFloat = UIScreen.main.bounds.size.height

class Utility: NSObject {
   //Present Alert controller with title, message and events
   static var alert = UIAlertController()
   static func alertContoller(title: String, message: String, actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, firstActoin: Selector?,secondActoin: Selector?,thirdActoin: Selector?, controller: UIViewController) {
       alert = UIAlertController(title: title, message: message, preferredStyle: .alert) // 1
       if(!actionTitleFirst.isEmpty) {
           let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
               if(firstActoin != nil){
                   controller.perform(firstActoin)
               }
           }
           alert.addAction(firstButtonAction)
       }
       
       if(!actionTitleSecond.isEmpty) {
           let secondAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
               //            NSLog("You pressed button two")
               if(secondActoin != nil){
                   controller.perform(secondActoin)
               }
           }
           alert.addAction(secondAction)
       }
       
       if(!actionTitleThird.isEmpty) {
           let thirdAction = UIAlertAction(title: actionTitleThird, style: .default) { (alert: UIAlertAction!) -> Void in
               //            NSLog("You pressed button two")
           }
           alert.addAction(thirdAction)
       }
       controller.present(alert, animated: true, completion:nil)
   }
   
   //Present Alert with title and message
   static func alert(message: String, title: String ,controller: UIViewController) {
       let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
       let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
       alertController.addAction(OKAction)
       controller.present(alertController, animated: true, completion:nil)
   }
   
   static func isNetworkAvailable(controller: UIViewController) -> Bool{
       if !Reachability.isConnectedToNetwork(){
           Utility.alert(message: "Please check your network connection.", title: "", controller: controller)
           return false
       }else{
           return true
       }
   }
   
   
}

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var VshadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = VshadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

class HomeConstant {
    static let totalItem: CGFloat = 4
    static let column: CGFloat = 3
    static let minLineSpacing: CGFloat = 3.0
    static let minItemSpacing: CGFloat = 3.0
    static let offset: CGFloat = 3.0 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        return totalWidth / column
    }
}


extension UICollectionViewCell {
    func roundCell(_ cell:UIView) {
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.white
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.30, height: 1.30)
        self.layer.shadowOpacity = 0.25
        self.layer.shadowRadius = 1.3
        self.layer.masksToBounds = true
        self.clipsToBounds = false
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0).cgColor
    }
}
extension NSObject {
    func safeValue(forKey key: String) -> Any? {
        let copy = Mirror(reflecting: self)
        for child in copy.children.makeIterator() {
            if let label = child.label, label == key {
                return child.value
            }
        }
        return nil
    }
}




