//
//  bookViewController.swift
//  BookLibraryCodingChallenge
//
//  Created by Mohit Gupta on 20/06/1942 Saka.
//  Copyright © 1942 Mohit Gupta. All rights reserved.
//

import UIKit

class bookViewController: UIViewController {
    var currentObject: result?
    var bannerData = [String]()
    
    var filtered:[String] = []
    var searchActive : Bool = false
    let searchController = UISearchController(searchResultsController: nil)
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    func configureSearchController(){
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search..."
        searchController.searchBar.sizeToFit()
        searchController.searchBar.becomeFirstResponder()
        self.navigationItem.titleView = searchController.searchBar
        
        self.definesPresentationContext = true
        
        
        
        self.searchController.searchBar.setSearchFieldBackgroundImage(UIImage(named: "SearchFieldBackground"), for: UIControl.State.normal)
        
        
        let textFieldInsideSearchBar = self.searchController.searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.textColor = .black
    }
    
    func customizeSearchBar()
    {
        for subview in searchController.searchBar.subviews
        {
            for view in subview.subviews
            {
                if let searchField = view as? UITextField
                {
                    let imageView = UIImageView()
                    let image = UIImage(named: "SearchBarIcon.png")
                    
                    imageView.image = image;
                    imageView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
                    
                    /*imageView.frame = CGRectMake(100, 0, 20, 19)*/
                    
                    searchField.leftView = imageView
                    searchField.leftViewMode = UITextField.ViewMode.always
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Subject"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont(name: appFont, size: appHeadingFontSize)!]
        
        self.configureSearchController()
    }
    
    
    
}

extension bookViewController:UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        let searchString = searchController.searchBar.text
        
        filtered = bannerData.filter({ (item) -> Bool in
            let countryText: NSString = item as NSString
            
            return (countryText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        collectionView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        collectionView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        collectionView.reloadData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if !searchActive {
            searchActive = true
            collectionView.reloadData()
        }
        
        searchController.searchBar.resignFirstResponder()
    }
    
}

extension bookViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellWidth = (width - 30) / 3 // compute your cell width
        return CGSize(width: cellWidth, height: cellWidth / 0.6)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
}

extension bookViewController: UICollectionViewDataSource,UICollectionViewDelegate  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchActive {
            return filtered.count
        }
        else {
            return bannerData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CustomCollectionCell
        self.configureCell(cell: cell)
        
        //        cell.wrapImage(img: self.currentObject?.formats["image/jpeg"])
        cell.wrapData(selectedObject: self.bannerData[indexPath.row], currentObj: currentObject!)
        return cell
    }
    
    func configureCell(cell: CustomCollectionCell){
        cell.contentView.layer.cornerRadius = 7.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.cornerRadius = 7.0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if let editAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "webViewController") as? webViewController {
            
            if let str = currentObject?.formats["text/html; charset=utf-8"]{
                editAddressVC.UrlStr = str
            }
            
            self.navigationController?.pushViewController(editAddressVC, animated: true)
            
            
        }
    }
}


class CustomCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet var ImageView: UIImageView!{
        didSet{
            self.ImageView.layer.cornerRadius = self.ImageView.frame.width/16.0
            self.ImageView.layer.masksToBounds = true
            
        }
    }
    
    @IBOutlet var titleLbl: UILabel!{
        didSet {
            self.titleLbl.font = UIFont(name: appFont, size: appFontSize)
            self.titleLbl.text = self.titleLbl.text
        }
    }
    
    @IBOutlet var nameLbl: UILabel!{
        didSet {
            self.nameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.nameLbl.text = self.nameLbl.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
    func wrapData(selectedObject: String,currentObj: result){
        self.titleLbl.text = selectedObject
        
        if let name = currentObj.title as? String{
            self.nameLbl.text = name
        }
        
        let arrays: [String:String] = currentObj.formats
        var formattedDictionary = [String:String]()
        for (key, value) in arrays {
            formattedDictionary[key.replacingOccurrences(of: "-", with: "")] = value
        }
        print(formattedDictionary)
        
        if let myString = currentObj.formats["image/jpeg"], !myString.isEmpty {
            //            self.ImageView.load(url: URL(string: myString))
            
            guard let imageUrl:URL = URL(string: myString) else {
                return
            }
            ImageView.loadImge(withUrl: imageUrl)
            
        }
        
        
    }
    
    
    func wrapImage(img: String?){
        
        if let myString = img, !myString.isEmpty {
            //            self.ImageView.load(url: URL(string: myString))
            
            guard let imageUrl:URL = URL(string: "http://www.gutenberg.org/cache/epub/74/pg74.cover.medium.jpg") else {
                return
            }
            ImageView.loadImge(withUrl: imageUrl)
            
        }
        
        //
    }
}


extension UIImageView {
    func loadImge(withUrl url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let imageData = try? Data(contentsOf: url) {
                if let image = UIImage(data: imageData) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

